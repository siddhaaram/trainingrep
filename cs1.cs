//Testing code change
//$c11 RXG 08/02/18 SPR 1097708: use StopTimer() after ForceRebuild3 call.
//$c10 RXG 07/03/18 SPR 1092832: Don't round-off  volume and Mass values.
//$c9 RXG 06/01/18 SPR 1081481:centerOfMassTolerance tolerance requirement changed.
//$c8 RXG 05/30/18 SPR 1083453:set precision for center of mass. 
//$c7 RXG 12/14/17 PRJ 23369 & SPR 1049945:swap Volume & Area values as per xml.
//$c6 AAD 07/27/15 Prj 21491 SPR 901771 Removing option for Verification on Rebuild.
//$c5 AAD 07/24/15 // Prj 21491 SPR 896889 : Setting Timeout value for Upgrade Assistant
//$c4 AAD 07/09/15 Proj:21491 Handling Future version Error,RealView turnedoff
//$c3 RXG 12/13/13 SPR 749181: added reference params added to opendoc6.
//$c2  10/18/2013 RXG Quality Checker xml report fixed.
//$c1  10/10/2013 RXG created
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;


using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using SolidWorks.Interop.swdocumentmgr;


namespace swBackOfficeTask
{
    class clsQualityCheckTask
    {
        [System.Runtime.InteropServices.DllImport("winmm.dll", EntryPoint = "timeGetTime", ExactSpelling = true, CharSet = System.Runtime.InteropServices.CharSet.Ansi, SetLastError = true)]
        private static extern int timeGetTime();

        private SldWorks m_swApp;

		//proj:21491 Turnoff RealView
		private MaterialVisualPropertiesData m_matVisProps;
		private PartDoc m_swPart;

        private ModelDoc2 m_swModelDoc;
        private clsLogger m_debugLogger;
        private clsLogger m_warningLogger;
        private bool m_bActivateAllConfigs;
		//Prj 21491 SPR 901771 Verification on Rebuild removed
        //private bool m_bVerificationOnRebuild;

        private string errorFilePath = null;
        private string finishedFilePath = null;

        private int m_activeSWVersion;

        private string m_workingDir; //backoffice working directory
        private string m_dataSrcDir; //root folder for all proxy data
        private string m_resultDir; //root folder for all quality check results
        private string m_resultDataDir; //result folder for this run of the quality check

        private bool m_dirtyFlag;
        private short m_phase;
        private string m_origFilePath;
        private string m_proxyFilePath;
        private string m_fileName;
        private string m_fileType;
        private int m_fileSize;

		//Prj 21491 SPR 896889: Timeout for upgrade Assistant
		private int m_timeout;

        //Private m_startTimeDictionary As Scripting.Dictionary
        private Hashtable m_startTimeDictionary;
        //Private m_endTimeDictionary As Scripting.Dictionary
        private Hashtable m_endTimeDictionary;

        private SwDMApplication m_swDocMgr;
        private MSXML2.DOMDocument60 m_xmlDoc;
        private MSXML2.IXMLDOMNode m_xmlRootNode;

        private const string OPEN_DOC_TIMER = "OpenDoc";
        private const string SAVE_DOC_TIMER = "SaveDoc";
        private const string REBUILD_DOC_TIMER = "RebuildDoc";

        private const string UNIT_BYTES = "bytes";
        private const string UNIT_KILOBYTES = "kilobytes";
        private const string UNIT_MEGABYTES = "megabytes";
        private const string UNIT_GIGABYTES = "gigabytes";

        private int m_bmpWidth;
        private int m_bmpHeight;
        private const int MIN_BMP_WIDTH = 100;
        private const int MAX_BMP_WIDTH = 10000;
        private const int MIN_BMP_HEIGHT = 100;
        private const int MAX_BMP_HEIGHT = 10000;

        private const string UNIT_MILLISECONDS = "milliseconds";
        private const string UNIT_SECONDS = "seconds";
        private const string UNIT_MINUTES = "minutes";
        private const string UNIT_HOURS = "hours";
        private const string UNIT_DAYS = "days";
        private const string UNIT_YEARS = "years";

        private const string swSelSHEETS = "SHEET";
        private const short MAX_OPEN_CONFIGS = 15;
        private const short swPerformanceVerifyOnRebuild = 102;

        private bool ActivateAllConfigs(ref ModelDoc2 Part, ref string returnedErrString)
        {
		    bool tempActivateAllConfigs = false;
            modBOUtilities boutils = new modBOUtilities();
            dynamic CurConfig = null;

		    try
		    {
			    string[] Names = null;
			    string docNameIn = null;
			    string CurConfigName = null;
			    int numOfConfigurations = 0;
			    int errors = 0;
			    int warnings = 0;
			    short configsToSaveCount = 0;
			    short numConfigsPerSave = 0;

			    configsToSaveCount = 0;
			    numConfigsPerSave = MAX_OPEN_CONFIGS;

			    docNameIn = Part.GetPathName();
			    CurConfig = Part.GetActiveConfiguration();
			    CurConfigName = CurConfig.Name;

			    numOfConfigurations = Part.GetConfigurationCount();
			    Names = Part.GetConfigurationNames();
			    bool saveSucceeded = false;
			    for (int i = 0; i < numOfConfigurations; i++)
			    {
				    if (Names[i] != CurConfigName)
				    {
					    configsToSaveCount = Convert.ToInt16(configsToSaveCount + 1);
					    Part.ShowConfiguration(Names[i]);
					    m_swModelDoc.ForceRebuild3(false);

					    if (configsToSaveCount == numConfigsPerSave)
					    {
						    Part.ShowConfiguration(CurConfigName);

						    //save silent and get the errors and warnings
						    saveSucceeded = Part.Save3((int)swSaveAsOptions_e.swSaveAsOptions_Silent |(int) swSaveAsOptions_e.swSaveAsOptions_SaveReferenced | 
                                (int)swSaveAsOptions_e.swSaveAsOptions_UpdateInactiveViews, errors, warnings);

						    //don't update if the save failed
						    if (errors > 0)
						    {
							    //get the error string based on the errors
                                returnedErrString = boutils.GetErrorStringFromError(ref errors);
							    goto func_cleanup;
						    }

						    //clean up the document
						    Part = null;
						    CloseDocument(docNameIn);
						    if (OpenDoc(docNameIn,ref returnedErrString) == false)
						    {
							    if (returnedErrString == modErrorConstants.ERR_STRING_NO_ERROR)
							    {
								    returnedErrString = modErrorConstants.ERR_STRING_REOPEN_IN_SW_FAILED;
							    }
							    goto func_cleanup;
						    }
						    Part = m_swApp.ActivateDoc2(docNameIn, true, errors);
						    configsToSaveCount = 0;
					    }
				    }
			    }
			    Part.ShowConfiguration(CurConfigName);
			    tempActivateAllConfigs = true;

                return tempActivateAllConfigs;
		    }
		    catch
		    {
			    goto Err_ActivateConfigs;
		    }

        func_cleanup:
            CurConfig = null;
            return tempActivateAllConfigs;

            Err_ActivateConfigs:
		        tempActivateAllConfigs = false;
		        goto func_cleanup;
	    }

        public object AnalyzeDocument(short iPhase, string origFilePath, string proxyFilePath, ref string returnedErrString)
        {
		   object tempAnalyzeDocument = null;
           modBOUtilities utils = new modBOUtilities();
           modMainClassMod clsMod = new modMainClassMod();
           bool bOldVerifyOnRebuild = false;
           bool bOldLockRecentDocumentsList = false;
           SwDMDocument document = null;
           string debStr;

		    try
		    {
			    returnedErrString = modErrorConstants.ERR_STRING_NO_ERROR;

			    // For backwards compatability make sure we have set m_swApp
			    if (m_swApp == null)
			    {
                    m_swApp = (SldWorks)clsMod.CreateSWObject(true);
			    }

			    //Store the existing verification on rebuild setting and set the one specified for the upgrade task
				//Prj 21491 SPR 901771 Verification on Rebuild removed.
			    //bOldVerifyOnRebuild = m_swApp.GetUserPreferenceToggle(swPerformanceVerifyOnRebuild);
			   // m_swApp.SetUserPreferenceToggle(swPerformanceVerifyOnRebuild, m_bVerificationOnRebuild);

			    //This locks the recent docuement list in Solidworks so that the temp files created during
			    //testing will not populate the list at user runtime
			    bOldLockRecentDocumentsList = m_swApp.GetUserPreferenceToggle((int)swUserPreferenceToggle_e.swLockRecentDocumentsList);
			    m_swApp.SetUserPreferenceToggle((int)swUserPreferenceToggle_e.swLockRecentDocumentsList, true);

			    m_phase = iPhase;
			    m_origFilePath = origFilePath;
			    m_proxyFilePath = proxyFilePath;

			    m_fileName = m_origFilePath.Substring(m_origFilePath.LastIndexOf("\\") + 1);
                m_fileType = utils.GetFileType(m_origFilePath);

			    if (m_phase < 1 || m_phase > 2)
			    {
                    debStr = ("Illegal Phase Value: " + m_phase);
                    DebugOut(ref debStr);

				    returnedErrString = modErrorConstants.ERR_STRING_INTERNAL_ERROR;
				    goto func_cleanup;
			    }
                debStr = ("Phase: " + m_phase);
                DebugOut(ref debStr);

			    if (CreateResultFolder() == false)
			    {
                    debStr = ("Unable to Create Result Folder: " + m_resultDataDir);
                    DebugOut(ref debStr);
				    returnedErrString = modErrorConstants.ERR_STRING_INTERNAL_ERROR;
				    goto func_cleanup;
			    }

                m_swDocMgr = (SwDMApplication)clsMod.CreateSWDMObject();

			    if (m_swDocMgr == null)
			    {
				    returnedErrString = modErrorConstants.ERR_STRING_NO_DOCUMENT_MANAGER;
				    goto func_cleanup;
			    }

			    if (GetDocumentDM(ref m_proxyFilePath,ref document,ref returnedErrString) == false)
			    {
				    goto func_cleanup;
			    }

			    int documentVersion = 0;
			    documentVersion = document.GetVersion();
			    if (documentVersion <= 0)
			    {
				    returnedErrString = modErrorConstants.ERR_STRING_NO_DOCUMENT_VERSION;
				    goto func_cleanup;
			    }

                if (utils.GetSolidWorksVersion(ref m_swApp,ref m_activeSWVersion) == false)
			    {
				    returnedErrString = modErrorConstants.ERR_STRING_NO_SW_VERSION;
				    goto func_cleanup;
			    }
			    debStr = ("SW Version: " + m_activeSWVersion);
                DebugOut(ref debStr);

			    document.CloseDoc();
			    document = null;

			    if (documentVersion > m_activeSWVersion)
			    {
				    returnedErrString = modErrorConstants.ERR_STRING_FUTURE_VERSION;
					//Proj:21491 Handling Future version Error
					tempAnalyzeDocument = false;
				    goto func_cleanup;
			    }
			    else if (DoPreAnalysis(ref returnedErrString))
			    {
				    if (DoAnalysis(ref returnedErrString))
				    {
					    if (DoPostAnalysis(ref returnedErrString))
					    {
						    tempAnalyzeDocument = true;
					    }
					    else
					    {
                            debStr = ("DoPostAnalysis failed");
                            DebugOut(ref debStr);
					    }
				    }
				    else
				    {
                        debStr = ("DoAnalysis failed");
                        DebugOut(ref debStr);
				    }
			    }
			    else
			    {
                    debStr = ("DoPreAnalysis failed");
                    DebugOut(ref debStr);
			    }

                debStr = ("Completed phase");
                DebugOut(ref debStr);
			    goto func_cleanup;
		    }
		    catch
		    {
			    goto func_err_handler;
		    }

            func_err_handler:
		        tempAnalyzeDocument = false;

		        if (returnedErrString == modErrorConstants.ERR_STRING_NO_ERROR)
		        {
			        returnedErrString = Microsoft.VisualBasic.Information.Err().Description + " in AnalyzeDocument.";
		        }

            func_cleanup:
		        //Reset verify on rebuild setting and recent document list lock
		        if ((m_swApp == null) == false)
		        {
			        m_swApp.SetUserPreferenceToggle(swPerformanceVerifyOnRebuild, bOldVerifyOnRebuild);
			        m_swApp.SetUserPreferenceToggle((int)swUserPreferenceToggle_e.swLockRecentDocumentsList, bOldLockRecentDocumentsList);
		        }

		        if ((document == null) == false)
		        {
			        document.CloseDoc();
		        }
		        document = null;

		        CleanUp();
		    return tempAnalyzeDocument;
	    }

        private object CleanUp()
        {
            if ((m_swDocMgr == null) == false)
            {
                m_swDocMgr = null;
            }
            m_swModelDoc = null;
            m_swApp = null;
            return null;
        }

        private bool CloseAllDocuments(bool inIncludeUnsaved)
        {
            bool tempCloseAllDocuments = false;
            try
            {
                tempCloseAllDocuments = m_swApp.CloseAllDocuments(inIncludeUnsaved);
                goto func_cleanup;

            }
            catch
            {
                goto func_err_handler;
            }
        func_err_handler:
            tempCloseAllDocuments = false;

        func_cleanup:

            return tempCloseAllDocuments;
        }

        private bool CloseDocument(string inDocumentName)
        {
            bool tempCloseDocument = false;
            try
            {
                m_swApp.CloseDoc(inDocumentName);
                goto func_cleanup;
            }
            catch
            {
                goto func_err_handler;
            }
        func_err_handler:
            tempCloseDocument = false;

        func_cleanup:

            return tempCloseDocument;
        }

        private bool DoAnalysis(ref string returnedErrString)
        {
            bool tempDoAnalysis = false;
            string DebStr;
            modBOUtilities boutils = new modBOUtilities();
            try
            {
                returnedErrString = modErrorConstants.ERR_STRING_NO_ERROR;

                bool saveSucceeded = false;
                int errors = 0;
                int warnings = 0;

                if (InitXMLDoc() == false)
                {
                    tempDoAnalysis = false;
                    DebStr = ("InitXMLDoc failed in DoAnalysis");
                    DebugOut(ref DebStr);
                    goto func_cleanup;
                }

                CloseAllDocuments(true);

                DebStr = ("START:" + OPEN_DOC_TIMER + " = " + StartTimer(OPEN_DOC_TIMER));
                DebugOut(ref DebStr);

                if (OpenDoc(m_proxyFilePath,ref returnedErrString) == false)
                {
                    returnedErrString = modErrorConstants.ERR_STRING_REOPEN_IN_SW_FAILED;
                    tempDoAnalysis = false;
                    DebStr = ("OpenDoc failed in DoAnalysis");
                    DebugOut(ref DebStr);
                    goto func_cleanup;
                }
                DebStr = ("STOP:" + OPEN_DOC_TIMER + " = " + StopTimer(OPEN_DOC_TIMER));
                DebugOut(ref DebStr);

                m_swModelDoc = m_swApp.ActivateDoc(m_fileName);
                if (m_swModelDoc != null)
                {
                    m_dirtyFlag = m_swModelDoc.GetSaveFlag();

                    DebStr = ("START:" + REBUILD_DOC_TIMER + " = " + StartTimer(REBUILD_DOC_TIMER));
                    DebugOut(ref DebStr);

                    m_swModelDoc.ForceRebuild3(false);

                    DebStr = ("STOP:" + REBUILD_DOC_TIMER + " = " + StopTimer(REBUILD_DOC_TIMER));
                    DebugOut(ref DebStr);

                    //save silent and get the errors and warnings
                    DebStr = ("START:" + SAVE_DOC_TIMER + " = " + StartTimer(SAVE_DOC_TIMER));
                    DebugOut(ref DebStr);

                    saveSucceeded = m_swModelDoc.Save3((int)swSaveAsOptions_e.swSaveAsOptions_Silent |(int)swSaveAsOptions_e.swSaveAsOptions_SaveReferenced | (int)swSaveAsOptions_e.swSaveAsOptions_UpdateInactiveViews, errors, warnings);

                    DebStr = ("STOP:" + SAVE_DOC_TIMER + " = " + StopTimer(SAVE_DOC_TIMER));
                    DebugOut(ref DebStr);

                    DebStr = ("before GenerateModelDocumentXML");
                    DebugOut(ref DebStr);

                    object modelDoc = m_swModelDoc;
                    GenerateModelDocumentXML(ref m_swModelDoc, ref m_xmlDoc, ref m_xmlRootNode);

                    DebStr = ("after GenerateModelDocumentXML");
                    DebugOut(ref DebStr);

                    //do the extra stuff needed for the file type
                    if (m_fileType == "SW_PART_TYPE" || m_fileType == "SW_PART_TYPE_OLD")
                    {

                        if (GeneratePartXML(ref m_xmlDoc,ref m_xmlRootNode) == false)
                        {
                            DebStr = ("'*** WARNING - Fatal Error");
                            DebugOut(ref DebStr);
                            DebStr = ("'*** Failure Generating Part XML");
                            DebugOut(ref DebStr);
                        }

                    }
                    else if (m_fileType == "SW_ASSEMBLY_TYPE" || m_fileType == "SW_ASSEMBLY_TYPE_OLD")
                    {

                        if (GenerateAssemblyXML(ref m_xmlDoc,ref m_xmlRootNode) == false)
                        {
                            DebStr = ("'*** WARNING - Fatal Error");
                            DebugOut(ref DebStr);
                            DebStr = ("'*** Failure Generating Assembly XML");
                            DebugOut(ref DebStr);
                        }

                    }
                    else if (m_fileType == "SW_DRAWING_TYPE" || m_fileType == "SW_DRAWING_TYPE_OLD")
                    {

                        if (GenerateDrawingXML(ref m_xmlDoc,ref m_xmlRootNode) == false)
                        {
                            DebStr = ("'*** WARNING - Fatal Error");
                            DebugOut(ref DebStr);
                            DebStr = ("'*** Failure Generating Drawing XML");
                            DebugOut(ref DebStr);
                        }

                    }

                    //clean up the document
                    m_swModelDoc = null;
                    DebStr = ("closing document");
                    DebugOut(ref DebStr);
                    CloseDocument(m_fileName);

                    //don't update if the save failed
                    if (errors > 0)
                    {
                        tempDoAnalysis = false;
                        //get the error string based on the errors
                        returnedErrString = boutils.GetErrorStringFromError(ref errors);
                        goto func_cleanup;
                    }

                    tempDoAnalysis = true;

                    goto func_cleanup;
                }
                else
                {
                    returnedErrString = modErrorConstants.ERR_STRING_REOPEN_IN_SW_FAILED;
                    DebStr = ("DoAnalysis " + modErrorConstants.ERR_STRING_REOPEN_IN_SW_FAILED + ": " + m_proxyFilePath);
                    DebugOut(ref DebStr);
                    tempDoAnalysis = false;
                }
            }
            catch
            {
                goto func_err_handler;
            }
            func_err_handler:
                if (tempDoAnalysis == false)
                {
                    DebStr = ("Error: " + Microsoft.VisualBasic.Information.Err().Description + " in function DoAnalysis.");
                    DebugOut(ref DebStr);
                    CloseDocument(m_fileName);
                    if (returnedErrString == modErrorConstants.ERR_STRING_NO_ERROR)
                    {
                        returnedErrString = ("Error: " + Microsoft.VisualBasic.Information.Err().Description + " in function DoAnalysis.");
                    }
                }

            func_cleanup:
                m_swModelDoc = null;

            return tempDoAnalysis;
        }

        private bool DoPostAnalysis(ref string returnedErrString)
        {
	        bool tempDoPostAnalysis = false;
		    string ActiveErrorHandler = null;

		    try
		    {
			    ActiveErrorHandler = "error_handler";

			    tempDoPostAnalysis = false;
			    returnedErrString = modErrorConstants.ERR_STRING_NO_ERROR;

			    //Make sure the document is closed before getting the file size
			    CloseAllDocuments(true);

			    //Get the current file size
			    m_fileSize = GetFileLength(m_proxyFilePath);

			    ActiveErrorHandler = "skip_tmp_file";

			    int errors = 0;
			    int warnings = 0;
			    bool saveSucceeded = false;
			    int length = 0;
			    short i = 0;

			    //Resave the file a few times just to ensure a proper save size (this is necessary because there is a bug in file saves that doubles the file size on occasion)
			    if (OpenDoc(m_proxyFilePath,ref returnedErrString) == true)
			    {
				    m_swModelDoc = m_swApp.ActivateDoc(m_proxyFilePath);
				    if (m_swModelDoc != null)
				    {
					    for (i = 1; i <= 3; i++)
					    {
						    //save silent and get the errors and warnings
                            saveSucceeded = m_swModelDoc.Save3((int)swSaveAsOptions_e.swSaveAsOptions_Silent | (int)swSaveAsOptions_e.swSaveAsOptions_SaveReferenced |
                                (int)swSaveAsOptions_e.swSaveAsOptions_UpdateInactiveViews, errors, warnings);

						    if (saveSucceeded == true)
						    {
							    length = GetFileLength(m_proxyFilePath);

							    if (m_fileSize > length)
							    {
								    m_fileSize = length;
							    }
						    }
					    }
					    //clean up the document
					    m_swModelDoc = null;
					    CloseAllDocuments(true);
				    }
			    }

		    }
		    catch
		    {
			    switch (ActiveErrorHandler)
			    {
				    case "error_handler":
					    goto error_handler;
				    case "skip_tmp_file":
					    goto skip_tmp_file;
				    default:
					    throw;
			    }
		    }
            skip_tmp_file:
		        m_swModelDoc = null;
		        CloseAllDocuments(true);
		        tempDoPostAnalysis = GenerateXMLDoc(ref m_xmlDoc,ref m_xmlRootNode);

		    // Intentionally fall through to error handler
            error_handler:
		    if (tempDoPostAnalysis == false)
		    {
			    m_swModelDoc = null;
			    CloseAllDocuments(true);
			    if (returnedErrString == modErrorConstants.ERR_STRING_NO_ERROR)
			    {
				    returnedErrString = ("Error: " + Microsoft.VisualBasic.Information.Err().Description + " in function DoPostAnalysis.");
			    }
		    }

            func_cleanup:
		            m_swModelDoc = null;
		    return tempDoPostAnalysis;
	    }

        private bool DoPreAnalysis(ref string returnedErrString)
        {
            bool tempDoPreAnalysis = false;
            modBOUtilities utils = new modBOUtilities();
            clsApplication swboApp = null;
            try
            {
                returnedErrString = modErrorConstants.ERR_STRING_NO_ERROR;
                bool saveSucceeded = false;
                int errors = 0;
                int warnings = 0;
                short ret = 0;

                if (OpenDoc(m_proxyFilePath,ref returnedErrString) == false)
                {
                    if (returnedErrString == modErrorConstants.ERR_STRING_NO_ERROR)
                    {
                        returnedErrString = modErrorConstants.ERR_STRING_OPEN_IN_SW_FAILED;
                    }
                    tempDoPreAnalysis = false;
                    goto func_cleanup;
                }

                m_swModelDoc = m_swApp.ActivateDoc(m_fileName);
                if (m_swModelDoc != null)
                {
                    //do the extra stuff needed for the file type
                    if (m_fileType == "SW_DRAWING_TYPE" || m_fileType == "SW_DRAWING_TYPE_OLD")
                    {

                        swboApp = new clsApplication();
                        swboApp.ResultPath = m_workingDir;
                        swboApp.glbProcessID = 0;
                        swboApp.MultiSession = false;
                        ret = swboApp.ActivateSheets();

                        swboApp = null;
                    }
                    else if (m_fileType == "SW_PART_TYPE" || m_fileType == "SW_PART_TYPE_OLD" || m_fileType == "SW_ASSEMBLY_TYPE" || m_fileType == "SW_ASSEMBLY_TYPE_OLD")
                    {

						//Proj:21491 RealView turnedoff
						if (m_fileType == "SW_PART_TYPE" || m_fileType == "SW_PART_TYPE_OLD")
						{
							m_swPart = (PartDoc)m_swModelDoc;
							m_matVisProps = m_swPart.GetMaterialVisualProperties();
							if (m_matVisProps != null)
							{
								m_matVisProps.RealView = false;
								long longstatus = m_swPart.SetMaterialVisualProperties(m_matVisProps, (int)swInConfigurationOpts_e.swThisConfiguration, null);
							}
						}

                        if (m_bActivateAllConfigs)
                        {
                            if (ActivateAllConfigs(ref m_swModelDoc,ref returnedErrString) == false)
                            {
                                if (returnedErrString == modErrorConstants.ERR_STRING_NO_ERROR)
                                {
                                    //TODO: add warning/error that we failed to activate all configurations
                                }
                                else
                                {
                                    goto func_cleanup;
                                }
                            }
                        }
                    }

                    //save silent and get the errors and warnings
                    saveSucceeded = m_swModelDoc.Save3((int)swSaveAsOptions_e.swSaveAsOptions_Silent | (int)swSaveAsOptions_e.swSaveAsOptions_SaveReferenced |
                        (int)swSaveAsOptions_e.swSaveAsOptions_UpdateInactiveViews, errors, warnings);

                    //clean up the document
                    m_swModelDoc = null;
                    CloseDocument(m_fileName);

                    //don't update if the save failed
                    if (errors > 0)
                    {
                        tempDoPreAnalysis = false;
                        //get the error string based on the errors
                        returnedErrString = utils.GetErrorStringFromError(ref errors);
                        goto func_cleanup;
                    }

                    tempDoPreAnalysis = true;

                    goto func_cleanup;
                }
                else
                {
                    returnedErrString = modErrorConstants.ERR_STRING_OPEN_IN_SW_FAILED;
                    string debstr = ("DoPreAnalysis " + modErrorConstants.ERR_STRING_OPEN_IN_SW_FAILED + ": " + m_proxyFilePath);
                    DebugOut(ref debstr);
                    tempDoPreAnalysis = false;
                }
                // Intentionally fall through to error handler
            }
            catch
            {
                goto func_err_handler;
            }
            func_err_handler:
                if (tempDoPreAnalysis == false)
                {
                    CloseDocument(m_fileName);
                    if (returnedErrString == modErrorConstants.ERR_STRING_NO_ERROR)
                    {
                        returnedErrString = ("Error: " + Microsoft.VisualBasic.Information.Err().Description + " in function DoPreAnalysis.");
                    }
                }

            func_cleanup:
                swboApp = null;
                m_swModelDoc = null;

            return tempDoPreAnalysis;
        }

        private bool GetDocumentDM(ref string documentName, ref SwDMDocument document, ref string returnedErrString)
        {
            bool tempGetDocumentDM = false;
            SwDmDocumentOpenError res;
            SwDmDocumentType dt;
            string FileType = null;
            tempGetDocumentDM = false;
            modBOUtilities utils = new modBOUtilities();

            FileType = utils.GetFileType(documentName);
            if (FileType == "SW_PART_TYPE" || FileType == "SW_PART_TYPE_OLD")
            {
                dt = SwDmDocumentType.swDmDocumentPart;
            }
            else if (FileType == "SW_ASSEMBLY_TYPE" || FileType == "SW_ASSEMBLY_TYPE_OLD")
            {
                dt = SwDmDocumentType.swDmDocumentAssembly;
            }
            else if (FileType == "SW_DRAWING_TYPE" || FileType == "SW_DRAWING_TYPE_OLD")
            {
                dt = SwDmDocumentType.swDmDocumentDrawing;
            }
            else
            {
                dt = SwDmDocumentType.swDmDocumentUnknown;
            }

            document = m_swDocMgr.GetDocument(documentName, dt, true, out res);

            if (res == SwDmDocumentOpenError.swDmDocumentOpenErrorNone)
            {
                tempGetDocumentDM = true;
            }
            else
            {
                document = null;
                if (res == SwDmDocumentOpenError.swDmDocumentOpenErrorFileNotFound)
                {
                    returnedErrString = modErrorConstants.ERR_STRING_DOCUMENT_NOT_FOUND;
                }
                else if (res == SwDmDocumentOpenError.swDmDocumentOpenErrorNonSW)
                {
                    returnedErrString = modErrorConstants.ERR_STRING_NOT_SW_DOC;
                }
                else
                {
                    returnedErrString = modErrorConstants.ERR_STRING_OPEN_IN_SW_FAILED;
                }
            }
            return tempGetDocumentDM;
        }

        private int GetFileLength(string inFilePath)
        {
            int tempGetFileLength = 0;
            try
            {
                tempGetFileLength = -1;
                tempGetFileLength = (int)Microsoft.VisualBasic.FileSystem.FileLen(inFilePath);
            }
            catch
            {
                goto error_handler;
            }
        error_handler:
            return tempGetFileLength;
        }


        private bool CreateResultFolder()
        {
			bool tempCreateResultFolder = false;
		    try
		    {
			    short lenProxyPath = 0;
			    short lenDataSrcPath = 0;

			    lenProxyPath = Convert.ToInt16(m_proxyFilePath.Length);
			    lenDataSrcPath = Convert.ToInt16(m_dataSrcDir.Length);

			    if (lenProxyPath <= lenDataSrcPath)
			    {
				    var tempVar = "len of proxy is less than data src";
				    DebugOut(ref tempVar);
				    return tempCreateResultFolder;
			    }

			    if (m_proxyFilePath.IndexOf(m_dataSrcDir) + 1 != 1)
			    {
				    var tempVar2 = "proxy is same as data src";
				    DebugOut(ref tempVar2);
				    DebugOut(ref m_proxyFilePath);
				    DebugOut(ref m_dataSrcDir);
				    return tempCreateResultFolder;
			    }

			    string tmpFld = null;
			    tmpFld = SimulateRight.Right(m_proxyFilePath, lenProxyPath - lenDataSrcPath);

			    m_resultDataDir = m_resultDir + tmpFld.Substring(0, tmpFld.LastIndexOf("\\") + 1);

                return modDependentDocs.CreateNonExistingFolder(ref m_resultDataDir);
		    }
		    catch
		    {
			    goto func_err_handler;
		    }
            func_err_handler:

            return tempCreateResultFolder;
	    }

	    private bool OpenDoc(string docNameIn, ref string returnedErrString)
	    {
			bool tempOpenDoc = false;
            object swModelDoc = null;
            object Part = null;
		    try
		    {
			    returnedErrString = modErrorConstants.ERR_STRING_NO_ERROR;
			    string FileType = null;
			    int dummyRet = 0;
			    int longstatus = 0;
			    bool ret = false;
                modBOUtilities utils = new modBOUtilities();

                FileType = utils.GetFileType(docNameIn);
			    swModelDoc = m_swApp.GetOpenDocumentByName(docNameIn);
			    if (swModelDoc != null)
			    {
				    // File already open in SolidWorks
				    tempOpenDoc = true;
				    goto func_cleanup;
			    }

			    longstatus = 0;
			    if (FileType == "SW_PART_TYPE" || FileType == "SW_PART_TYPE_OLD")
			    {
                    Part = m_swApp.OpenDoc6(docNameIn, (int)swDocumentTypes_e.swDocPART, (int)swOpenDocOptions_e.swOpenDocOptions_Silent, "",ref dummyRet,ref longstatus);
			    }
			    else if (FileType == "SW_ASSEMBLY_TYPE" || FileType == "SW_ASSEMBLY_TYPE_OLD")
			    {
                    Part = m_swApp.OpenDoc6(docNameIn, (int)swDocumentTypes_e.swDocASSEMBLY, (int)swOpenDocOptions_e.swOpenDocOptions_Silent, "",ref dummyRet,ref longstatus);
			    }
			    else if (FileType == "SW_DRAWING_TYPE" || FileType == "SW_DRAWING_TYPE_OLD")
			    {
                    Part = m_swApp.OpenDoc6(docNameIn, (int)swDocumentTypes_e.swDocDRAWING, (int)swOpenDocOptions_e.swOpenDocOptions_Silent, "",ref dummyRet,ref longstatus);
			    }

			    ret = true;

                if ((longstatus & (int)swFileLoadWarning_e.swFileLoadWarning_ReadOnly) != 0)
			    {
				    returnedErrString = modErrorConstants.ERR_STRING_READONLY;
				    ret = false;
			    }
                else if ((longstatus & (int)swFileLoadWarning_e.swFileLoadWarning_SharingViolation) != 0)
			    {
				    returnedErrString = modErrorConstants.ERR_STRING_FILE_LOCKED;
				    ret = false;
			    }
                else if ((longstatus & (int)swFileLoadWarning_e.swFileLoadWarning_ViewOnlyRestrictions) != 0)
			    {
				    returnedErrString = modErrorConstants.ERR_STRING_VIEWONLY;
				    ret = false;
			    }
			    else if (Part == null)
			    {
				    returnedErrString = modErrorConstants.ERR_STRING_OPEN_IN_SW_FAILED;
				    ret = false;
			    }

			    if (ret == false || Part == null)
			    {
				    var tempVar = "OpenDoc: " + docNameIn + " ret = False error: " + returnedErrString;
				    DebugOut(ref tempVar);
				    tempOpenDoc = false;
			    }
			    else
			    {
				    tempOpenDoc = true;
			    }

			    goto func_cleanup;

		    }
		    catch
		    {
			    goto func_err_handler;
		    }
            func_err_handler:
		            CloseDocument(docNameIn);
		            returnedErrString = modErrorConstants.ERR_STRING_OPEN_IN_SW_FAILED;
		            tempOpenDoc = false;
		            var tempVar2 = "5555:  OpenDoc ERRORHANDLER " + docNameIn + " " + Microsoft.VisualBasic.Information.Err().Description;
		            DebugOut(ref tempVar2);

            func_cleanup:
		            Part = null;
		            swModelDoc = null;

		    return tempOpenDoc;
	    }
		//Prj 21491 SPR 901771 Verification on Rebuild removed
	    //public bool SetOptions(bool bActivateAllConfigsIn, bool bVerificationOnRebuild)
		public bool SetOptions(bool bActivateAllConfigsIn)
	    {
		    m_bActivateAllConfigs = bActivateAllConfigsIn;
			//Prj 21491 SPR 901771 Verification on Rebuild removed
		    //m_bVerificationOnRebuild = bVerificationOnRebuild;
		    return true;
	    }
		// Prj 21491 SPR 896889 : Setting Timeout value for Upgrade Assistant
		public void SetTimeout(int btimeout)
		{
			m_timeout = btimeout;
		}
		//Prj 21491 SPR 896889: Returning Timeout Value
		public int getTimeout()
		{
			return m_timeout;
		}

        public void SetSolidWorks(ref ISldWorks objSolidWorks)
	    {
		    m_swApp = null;
		    m_swApp = (SldWorks)objSolidWorks;
	    }

	    public bool SetBMPOptions(int bmpWidthIn, int bmpHeightIn)
	    {
		    bool tempSetBMPOptions = false;
		    tempSetBMPOptions = false;

		    if ((bmpWidthIn >= MIN_BMP_WIDTH && bmpWidthIn <= MAX_BMP_WIDTH) && (bmpHeightIn >= MIN_BMP_HEIGHT && bmpHeightIn <= MAX_BMP_HEIGHT))
		    {
			    m_bmpWidth = bmpWidthIn;
			    m_bmpHeight = bmpHeightIn;
			    tempSetBMPOptions = true;
		    }
		    return tempSetBMPOptions;
	    }


	    public void SetWorkingDirectory(ref string setIn)
	    {
		    m_workingDir = setIn;
	    }

	    public void SetDataSourceDirectory(ref string setIn)
	    {
		    m_dataSrcDir = setIn;
	    }

	    public void SetResultDirectory(ref string setIn)
	    {
		    m_resultDir = setIn;
	    }

        public void SetErrorFilePath(ref string setIn)
        {
            errorFilePath = setIn;
            m_warningLogger.FilePath = errorFilePath;
            //m_debugLogger.FilePath = errorFilePath;
	    }

	    private int StartTimer(string inTimerName)
	    {
		    try
		    {
			    int startTime = 0;
			    if (m_startTimeDictionary.Contains(inTimerName))
			    {
				    m_startTimeDictionary.Remove(inTimerName);
			    }

			    if (m_endTimeDictionary.Contains(inTimerName))
			    {
				    m_endTimeDictionary.Remove(inTimerName);
			    }

			    startTime = timeGetTime();
			    m_startTimeDictionary[inTimerName] = startTime;

			    return startTime;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:
		    return -1;
	    }

	    private int StopTimer(string inTimerName)
	    {
			int tempStopTimer = 0;
		    try
		    {
			    int endTime = 0;
			    tempStopTimer = -1;
			    endTime = timeGetTime();

			    if (m_startTimeDictionary.Contains(inTimerName))
			    {
				    m_endTimeDictionary[inTimerName] = endTime;
				    tempStopTimer = endTime;
			    }
			    return tempStopTimer;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:
		    return -1;
	    }

	    private int GetTimerChange(string inTimerName)
	    {
			int tempGetTimerChange = 0;
		    try
		    {
			    int endTime = 0;
			    int startTime = 0;
			    tempGetTimerChange = -1;

			    if (m_startTimeDictionary.Contains(inTimerName) && m_endTimeDictionary.Contains(inTimerName))
			    {
                    startTime = Convert.ToInt32( m_startTimeDictionary[inTimerName]);
				    endTime = Convert.ToInt32( m_endTimeDictionary[inTimerName]);
                    
				    if (endTime >= startTime)
				    {
					    tempGetTimerChange = endTime - startTime;
				    }
				    else
				    {
					    tempGetTimerChange = endTime + (0xFFFF - startTime);
				    }
			    }
			    return tempGetTimerChange;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:
		    return -1;
	    }

	    private bool DebugOut(ref string message)
	    {
		    return m_debugLogger.Log(message);
	    }

	    private bool InitXMLDoc()
	    {
			bool tempInitXMLDoc = false;
		    try
		    {
			    tempInitXMLDoc = false;

			    MSXML2.IXMLDOMProcessingInstruction xmlVersion = null;
			    MSXML2.IXMLDOMAttribute xmlRootXSI = null;
			    MSXML2.IXMLDOMAttribute xmlRootXSD = null;

			    m_xmlDoc = new MSXML2.DOMDocument60();
			    if (m_xmlDoc == null)
			    {
				    var tempVar = "Cannot create MSXML2.DOMDocument60";
				    DebugOut(ref tempVar);
			    }
			    m_xmlDoc.preserveWhiteSpace = false;
			    m_xmlDoc.resolveExternals = true;

			    xmlVersion = m_xmlDoc.createProcessingInstruction("xml", "version=" + (char)(34) + "1.0" + (char)(34) + " encoding=" + (char)(34) + "UTF-16" + (char)(34));
			    m_xmlDoc.appendChild(xmlVersion);
			    m_xmlRootNode = m_xmlDoc.createElement("sldSignature");

			    xmlRootXSI = m_xmlDoc.createAttribute("xmlns:xsi");
			    xmlRootXSI.value = "http://www.w3.org/2001/XMLSchema-instance";
			    m_xmlRootNode.attributes.setNamedItem(xmlRootXSI);

			    xmlRootXSD = m_xmlDoc.createAttribute("xmlns:xsd");
			    xmlRootXSD.value = "http://www.w3.org/2001/XMLSchema";
			    m_xmlRootNode.attributes.setNamedItem(xmlRootXSD);

			    m_xmlDoc.appendChild(m_xmlRootNode);

			    tempInitXMLDoc = true;

			    goto func_cleanup;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:
		        m_xmlRootNode = null;
		        m_xmlDoc = null;
		        var tempVar2 = "Error: " + Microsoft.VisualBasic.Information.Err().Description + " in function InitXMLDoc.";
		        DebugOut(ref tempVar2);
            func_cleanup:

            return tempInitXMLDoc;
	    }

        public static class SimulateRight
        {
	        public static string Right(string expression, int length)
	        {
		        if (string.IsNullOrEmpty(expression))
			        return string.Empty;
		        else if (length >= expression.Length)
			        return expression;
		        else
			        return expression.Substring(expression.Length - length);
	        }
        }

        private bool GenerateXMLDoc(ref MSXML2.DOMDocument60 xmlDoc, ref MSXML2.IXMLDOMNode xmlRootNode)
        {
			bool tempGenerateXMLDoc = false;
		    try
		    {
			    string xmlDocFilePath = null;

			    MSXML2.IXMLDOMNode fileInfoNode = null;
			    MSXML2.IXMLDOMNode fileNameNode = null;
			    MSXML2.IXMLDOMNode filePathNode = null;
			    MSXML2.IXMLDOMNode fileSizeNode = null;
			    MSXML2.IXMLDOMAttribute fileSizeUnitAttribute = null;
			    MSXML2.IXMLDOMNode fileTypeNode = null;
			    MSXML2.IXMLDOMNode fileVersionNode = null;
			    MSXML2.IXMLDOMNode proxyPathNode = null;

			    tempGenerateXMLDoc = false;

			    fileInfoNode = xmlDoc.createElement("FileInfo");
			    xmlRootNode.insertBefore(fileInfoNode, xmlRootNode.firstChild);

			    fileNameNode = xmlDoc.createElement("FileName");
			    fileNameNode.text = m_fileName;
			    fileInfoNode.appendChild(fileNameNode);

			    filePathNode = xmlDoc.createElement("FullPath");
			    filePathNode.text = m_origFilePath;
			    fileInfoNode.appendChild(filePathNode);

			    fileSizeNode = xmlDoc.createElement("FileSize");

			    fileSizeUnitAttribute = xmlDoc.createAttribute("unit");
			    fileSizeUnitAttribute.value = UNIT_BYTES;
			    fileSizeNode.attributes.setNamedItem(fileSizeUnitAttribute);

			    fileSizeNode.text = Convert.ToString(m_fileSize);
			    fileInfoNode.appendChild(fileSizeNode);

			    fileTypeNode = xmlDoc.createElement("FileType");

			    if (m_fileType == "SW_DRAWING_TYPE" || m_fileType == "SW_DRAWING_TYPE_OLD")
			    {
				    fileTypeNode.text = "Drawing";
			    }
			    else if (m_fileType == "SW_PART_TYPE" || m_fileType == "SW_PART_TYPE_OLD")
			    {
				    fileTypeNode.text = "Part";
			    }
			    else if (m_fileType == "SW_ASSEMBLY_TYPE" || m_fileType == "SW_ASSEMBLY_TYPE_OLD")
			    {
				    fileTypeNode.text = "Assembly";
			    }
			    else
			    {
				    fileTypeNode.text = "Unknown";
			    }
			    fileInfoNode.appendChild(fileTypeNode);

			    fileVersionNode = xmlDoc.createElement("FileVersion");
			    fileVersionNode.text = Convert.ToString(m_activeSWVersion);
			    fileInfoNode.appendChild(fileVersionNode);

			    proxyPathNode = xmlDoc.createElement("ProxyPath");
			    proxyPathNode.text = m_proxyFilePath;
			    fileInfoNode.appendChild(proxyPathNode);

			    GenerateQualityMetricsXML(ref xmlDoc,ref xmlRootNode);

			    xmlDocFilePath = m_resultDataDir + m_fileName + "__" + m_phase + ".xml";
			    xmlDoc.save(xmlDocFilePath);

			    tempGenerateXMLDoc = true;

			    goto func_cleanup;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:

            func_cleanup:

            return tempGenerateXMLDoc;
	    }

	    private bool GeneratePartXML(ref MSXML2.DOMDocument60 xmlDoc, ref MSXML2.IXMLDOMNode xmlRootNode)
	    {
			bool tempGeneratePartXML = false;
            string debstr;
            modBOUtilities utils = new modBOUtilities();
		    try
		    {
			    //PartInfo
			    dynamic defaultConfiguration = null;
			    string defaultConfigurationName = null;
			    int i = 0;
			    int configurationCount = 0;
			    string[] configurationNames = null;
			    string returnedErrString = null;
			    returnedErrString = modErrorConstants.ERR_STRING_NO_ERROR;

			    MSXML2.IXMLDOMNode partInfoNode = null;
			    MSXML2.IXMLDOMNode configurationNodes = null;
			    MSXML2.IXMLDOMNode configurationCountNode = null;

			    tempGeneratePartXML = false;

			    partInfoNode = xmlDoc.createElement("PartInfo");
			    xmlRootNode.appendChild(partInfoNode);

			    configurationNodes = xmlDoc.createElement("Configurations");
			    partInfoNode.appendChild(configurationNodes);

			    configurationCount = m_swModelDoc.GetConfigurationCount();

			    configurationCountNode = xmlDoc.createElement("NumConfigurations");
			    configurationCountNode.text = Convert.ToString(configurationCount);
			    configurationNodes.appendChild(configurationCountNode);

			    defaultConfiguration = m_swModelDoc.GetActiveConfiguration();
			    defaultConfigurationName = defaultConfiguration.Name;
			    configurationNames = m_swModelDoc.GetConfigurationNames();

			    short configsToSaveCount = 0;
			    short numConfigsPerSave = 0;
			    string configurationName = null;
			    bool saveSucceeded = false;
			    int errors = 0;
			    int warnings = 0;
			    if (m_bActivateAllConfigs == true)
			    {
	                configsToSaveCount = 0;
	                numConfigsPerSave = MAX_OPEN_CONFIGS;

				    for (i = 0; i < configurationCount; i++)
				    {
					    configsToSaveCount = Convert.ToInt16(configsToSaveCount + 1);
					    configurationName = (string)configurationNames[i];
					    m_swModelDoc.ShowConfiguration(configurationName);

					    debstr = ("START:" + REBUILD_DOC_TIMER + " = " + StartTimer(REBUILD_DOC_TIMER));
                        DebugOut(ref debstr);

					    m_swModelDoc.ForceRebuild3(false);
                        debstr = ("STOP:" + REBUILD_DOC_TIMER + " = " + StopTimer(REBUILD_DOC_TIMER));
                        DebugOut(ref debstr);

                        ModelDoc2 modelDoc = m_swModelDoc;
                        GeneratePartConfigurationXML(ref modelDoc, ref xmlDoc, ref configurationNodes, defaultConfigurationName);

					    if (configsToSaveCount == numConfigsPerSave)
					    {
						    m_swModelDoc.ShowConfiguration(defaultConfigurationName);

						    //save silent and get the errors and warnings
                            saveSucceeded = m_swModelDoc.Save3((int)swSaveAsOptions_e.swSaveAsOptions_Silent | (int)swSaveAsOptions_e.swSaveAsOptions_SaveReferenced | (int)swSaveAsOptions_e.swSaveAsOptions_UpdateInactiveViews, errors, warnings);

						    //don't update if the save failed
						    if (errors > 0)
						    {
							    //get the error string based on the errors
                                returnedErrString = utils.GetErrorStringFromError(ref errors);
							    goto func_cleanup;
						    }

						    //clean up the document
						    m_swModelDoc = null;
						    CloseDocument(m_fileName);
						    if (OpenDoc(m_proxyFilePath,ref returnedErrString) == false)
						    {
							    if (returnedErrString == modErrorConstants.ERR_STRING_NO_ERROR)
							    {
								    returnedErrString = modErrorConstants.ERR_STRING_REOPEN_IN_SW_FAILED;
							    }
							    goto func_cleanup;
						    }
						    m_swModelDoc = m_swApp.ActivateDoc2(m_fileName, true, errors);
						    configsToSaveCount = 0;
					    }
				    }
				    m_swModelDoc.ShowConfiguration(defaultConfigurationName);
			    }
			    else
			    {
                    ModelDoc2 modelDoc = m_swModelDoc;
				    GeneratePartConfigurationXML(ref modelDoc, ref xmlDoc, ref configurationNodes, defaultConfigurationName);
			    }
			    tempGeneratePartXML = true;
			    goto func_cleanup;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:

            func_cleanup:

            return tempGeneratePartXML;
	    }

	    private bool GeneratePartConfigurationXML(ref ModelDoc2 mDoc, ref MSXML2.DOMDocument60 xmlDoc, ref MSXML2.IXMLDOMNode xmlParentNode, string defaultConfigurationName)
	    {
			bool tempGeneratePartConfigurationXML = false;
            modBOUtilities utils = new modBOUtilities();
		    try
		    {
			    MSXML2.IXMLDOMNode configurationNode = null;
			    MSXML2.IXMLDOMAttribute configurationNameAttribute = null;
			    MSXML2.IXMLDOMNode configurationNameNode = null;
			    MSXML2.IXMLDOMNode configurationIsActiveNode = null;
			    MSXML2.IXMLDOMNode configurationIsDerivedNode = null;
			    dynamic configuration = null;
			    string configurationName = null;
			    MSXML2.IXMLDOMNode componentCountNode = null;
			    MSXML2.IXMLDOMNode componentInfoNode = null;

			    configuration = mDoc.GetActiveConfiguration();
			    configurationName = configuration.Name;
			    configurationNode = xmlDoc.createElement("Configuration");
			    configurationNameAttribute = xmlDoc.createAttribute("name");
			    configurationNameAttribute.value = configurationName;
			    configurationNode.attributes.setNamedItem(configurationNameAttribute);

			    xmlParentNode.appendChild(configurationNode);

			    configurationNameNode = xmlDoc.createElement("ConfigurationName");
			    configurationNameNode.text = configurationName;
			    configurationNode.appendChild(configurationNameNode);

			    configurationIsActiveNode = xmlDoc.createElement("IsActive");
                configurationIsActiveNode.text = utils.xmlBool(Convert.ToBoolean(configurationName == defaultConfigurationName));
			    configurationNode.appendChild(configurationIsActiveNode);

			    configuration = mDoc.GetConfigurationByName(configurationName);

			    configurationIsDerivedNode = xmlDoc.createElement("IsDerived");
                configurationIsDerivedNode.text = utils.xmlBool(configuration.IsDerived());
			    configurationNode.appendChild(configurationIsDerivedNode);

                object doc = mDoc;
                GenerateModelFeaturesXML(ref mDoc, ref xmlDoc, ref configurationNode);

			    componentInfoNode = xmlDoc.createElement("Components");

			    componentCountNode = xmlDoc.createElement("NumComponents");
			    componentCountNode.text = Convert.ToString(0);
			    componentInfoNode.appendChild(componentCountNode);

			    configurationNode.appendChild(componentInfoNode);

			    GenerateRebuildDocumentTimerXML(ref xmlDoc,ref configurationNode);

                //PartDoc prtdoc = (PartDoc)mDoc;
                GenerateTessellationTrianglesCountXML(ref mDoc, ref xmlDoc, ref configurationNode);

			    GenerateMassPropertiesXML(ref mDoc,ref xmlDoc,ref configurationNode);

			    tempGeneratePartConfigurationXML = true;
			    goto func_cleanup;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:
            func_cleanup:
            return tempGeneratePartConfigurationXML;
	    }

        private bool GenerateAssemblyXML(ref MSXML2.DOMDocument60 xmlDoc, ref MSXML2.IXMLDOMNode xmlRootNode)
        {
			bool tempGenerateAssemblyXML = false;
            modBOUtilities boutils = new modBOUtilities();
		    try
		    {
			    //AssemblyInfo
			    dynamic defaultConfiguration = null;
			    string defaultConfigurationName = null;
			    int i = 0;
			    int configurationCount = 0;
			    string[] configurationNames = null;
			    string returnedErrString = null;
			    returnedErrString = modErrorConstants.ERR_STRING_NO_ERROR;

			    MSXML2.IXMLDOMNode asmInfoNode = null;
			    MSXML2.IXMLDOMNode configurationNodes = null;
			    MSXML2.IXMLDOMNode configurationCountNode = null;

			    tempGenerateAssemblyXML = false;

			    asmInfoNode = xmlDoc.createElement("AssemblyInfo");
			    xmlRootNode.appendChild(asmInfoNode);

			    configurationNodes = xmlDoc.createElement("Configurations");
			    asmInfoNode.appendChild(configurationNodes);

			    configurationCount = m_swModelDoc.GetConfigurationCount();

			    configurationCountNode = xmlDoc.createElement("NumConfigurations");
			    configurationCountNode.text = Convert.ToString(configurationCount);
			    configurationNodes.appendChild(configurationCountNode);

			    defaultConfiguration = m_swModelDoc.GetActiveConfiguration();
			    defaultConfigurationName = defaultConfiguration.Name;
			    configurationNames = (string[])m_swModelDoc.GetConfigurationNames();

			    short configsToSaveCount = 0;
			    short numConfigsPerSave = 0;
			    string configurationName = null;
			    bool saveSucceeded = false;
			    int errors = 0;
			    int warnings = 0;
			    if (m_bActivateAllConfigs == true)
			    {
	                configsToSaveCount = 0;
	                numConfigsPerSave = MAX_OPEN_CONFIGS;

				    for (i = 0; i < configurationCount; i++)
				    {
					    configsToSaveCount = Convert.ToInt16(configsToSaveCount + 1);
					    configurationName = configurationNames[i];
					    m_swModelDoc.ShowConfiguration(configurationName);

                        string debstr = string.Format("START:{0} = {1}", REBUILD_DOC_TIMER, StartTimer(REBUILD_DOC_TIMER));
                        DebugOut(ref debstr);

					    m_swModelDoc.ForceRebuild3(false);
                        //SPR 1097708: use StopTimer() after ForceRebuild3 call. 
                        debstr = string.Format("STOP:{0} = {1}", REBUILD_DOC_TIMER, StopTimer(REBUILD_DOC_TIMER));
                        DebugOut(ref debstr);

					    GenerateAssemblyConfigurationXML(ref m_swModelDoc, ref xmlDoc, ref configurationNodes, defaultConfigurationName);

					    if (configsToSaveCount == numConfigsPerSave)
					    {
						    m_swModelDoc.ShowConfiguration(defaultConfigurationName);
						    //save silent and get the errors and warnings
                            saveSucceeded = m_swModelDoc.Save3((int)swSaveAsOptions_e.swSaveAsOptions_Silent | (int)swSaveAsOptions_e.swSaveAsOptions_SaveReferenced | (int)swSaveAsOptions_e.swSaveAsOptions_UpdateInactiveViews, errors, warnings);

						    //don't update if the save failed
						    if (errors > 0)
						    {
							    //get the error string based on the errors
                                returnedErrString = boutils.GetErrorStringFromError(ref errors);
							    goto func_cleanup;
						    }

						    //clean up the document
						    m_swModelDoc = null;
						    CloseDocument(m_fileName);
						    if (OpenDoc(m_proxyFilePath,ref returnedErrString) == false)
						    {
							    if (returnedErrString == modErrorConstants.ERR_STRING_NO_ERROR)
							    {
								    returnedErrString = modErrorConstants.ERR_STRING_REOPEN_IN_SW_FAILED;
							    }
							    goto func_cleanup;
						    }
						    m_swModelDoc = m_swApp.ActivateDoc2(m_fileName, true, errors);
						    configsToSaveCount = 0;
					    }
				    }
				    m_swModelDoc.ShowConfiguration(defaultConfigurationName);
			    }
			    else
			    {
				    GenerateAssemblyConfigurationXML(ref m_swModelDoc, ref xmlDoc, ref configurationNodes, defaultConfigurationName);
			    }
			    tempGenerateAssemblyXML = true;
			    goto func_cleanup;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:
            func_cleanup:
            return tempGenerateAssemblyXML;
	    }

	    private bool GenerateAssemblyConfigurationXML(ref ModelDoc2 mDoc, ref MSXML2.DOMDocument60 xmlDoc, ref MSXML2.IXMLDOMNode xmlParentNode, string defaultConfigurationName)
	    {
			bool tempGenerateAssemblyConfigurationXML = false;
            modBOUtilities utils = new modBOUtilities();
		    try
		    {
			    tempGenerateAssemblyConfigurationXML = false;

			    MSXML2.IXMLDOMNode configurationNode = null;
			    MSXML2.IXMLDOMAttribute configurationNameAttribute = null;
			    MSXML2.IXMLDOMNode configurationNameNode = null;
			    MSXML2.IXMLDOMNode configurationIsActiveNode = null;
			    MSXML2.IXMLDOMNode configurationIsDerivedNode = null;
                Configuration configuration = null;
			    string configurationName = null;

			    configuration = mDoc.GetActiveConfiguration();
			    configurationName = configuration.Name;
			    configurationNode = xmlDoc.createElement("Configuration");
			    configurationNameAttribute = xmlDoc.createAttribute("name");
			    configurationNameAttribute.value = configurationName;
			    configurationNode.attributes.setNamedItem(configurationNameAttribute);

			    xmlParentNode.appendChild(configurationNode);

			    configurationNameNode = xmlDoc.createElement("ConfigurationName");
			    configurationNameNode.text = configurationName;
			    configurationNode.appendChild(configurationNameNode);

			    configurationIsActiveNode = xmlDoc.createElement("IsActive");
                configurationIsActiveNode.text = utils.xmlBool(Convert.ToBoolean(configurationName == defaultConfigurationName));
			    configurationNode.appendChild(configurationIsActiveNode);
			    configuration = mDoc.GetConfigurationByName(configurationName);
			    configurationIsDerivedNode = xmlDoc.createElement("IsDerived");
                configurationIsDerivedNode.text = utils.xmlBool(Convert.ToBoolean(configuration.IsDerived()));
			    configurationNode.appendChild(configurationIsDerivedNode);

			    GenerateModelFeaturesXML(ref mDoc,ref xmlDoc,ref configurationNode);

			    GenerateComponentsXML(ref configuration,ref xmlDoc,ref configurationNode);

			    GenerateRebuildDocumentTimerXML(ref xmlDoc,ref configurationNode);

                //PartDoc partDocument = new PartDoc();
                //partDocument = (PartDoc)mDoc;

                GenerateTessellationTrianglesCountXML(ref mDoc, ref xmlDoc, ref configurationNode);

			    GenerateMassPropertiesXML(ref mDoc,ref xmlDoc,ref configurationNode);

			    tempGenerateAssemblyConfigurationXML = true;
			    goto func_cleanup;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:
            func_cleanup:
            return tempGenerateAssemblyConfigurationXML;
	    }

	    private bool GenerateDrawingXML(ref MSXML2.DOMDocument60 xmlDoc, ref MSXML2.IXMLDOMNode xmlRootNode)
	    {
			bool tempGenerateDrawingXML = false;
            modBOUtilities utils = new modBOUtilities();
		    try
		    {
			    //DrawingInfo
			    dynamic defaultSheet = null;
			    string defaultSheetName = null;
			    int i = 0;
			    int SheetCount = 0;
			    string[] sheetNames = null;
                IDrawingDoc drawDocument;
                drawDocument = (IDrawingDoc)m_swModelDoc;


			    MSXML2.IXMLDOMNode drawingInfoNode = null;
			    MSXML2.IXMLDOMNode sheetNodes = null;
			    MSXML2.IXMLDOMNode sheetCountNode = null;

			    tempGenerateDrawingXML = false;

			    drawingInfoNode = xmlDoc.createElement("DrawingInfo");
			    xmlRootNode.appendChild(drawingInfoNode);

			    sheetNodes = xmlDoc.createElement("Sheets");
			    drawingInfoNode.appendChild(sheetNodes);

                SheetCount = drawDocument.GetSheetCount();

			    sheetCountNode = xmlDoc.createElement("NumSheets");
			    sheetCountNode.text = Convert.ToString(SheetCount);
			    sheetNodes.appendChild(sheetCountNode);

                defaultSheet = drawDocument.GetCurrentSheet();
			    defaultSheetName = defaultSheet.getName();
                sheetNames = (string[])drawDocument.GetSheetNames();

			    MSXML2.IXMLDOMNode sheetNode = null;
			    MSXML2.IXMLDOMAttribute sheetNameAttribute = null;
			    MSXML2.IXMLDOMNode sheetNameNode = null;
			    MSXML2.IXMLDOMNode sheetIsActiveNode = null;
			    MSXML2.IXMLDOMNode sheetBMPNode = null;
			    string sheetName = null;
			    string sheetBMPPath = null;
			    bool bBMPCreatedFlag = false;
			    for (i = 0; i < SheetCount; i++)
			    {
				    sheetName = sheetNames[i];
				    sheetNode = xmlDoc.createElement("Sheet");
				    sheetNameAttribute = xmlDoc.createAttribute("name");
				    sheetNameAttribute.value = sheetName;
				    sheetNode.attributes.setNamedItem(sheetNameAttribute);
				    sheetNodes.appendChild(sheetNode);

				    sheetNameNode = xmlDoc.createElement("SheetName");
				    sheetNameNode.text = sheetName;
				    sheetNode.appendChild(sheetNameNode);

				    sheetIsActiveNode = xmlDoc.createElement("IsActive");
                    sheetIsActiveNode.text = utils.xmlBool(Convert.ToBoolean(sheetName == defaultSheetName));
				    sheetNode.appendChild(sheetIsActiveNode);

                    drawDocument.ActivateSheet(sheetName);

                    GenerateModelFeaturesXML(ref m_swModelDoc, ref xmlDoc, ref sheetNode);

                    //RXG -PRJ 15637:
                    //m_swApp.ActiveDoc.SetFeatureManagerWidth(0);
				    //m_swApp.ActiveDoc.ActiveView.FrameState = 0;
				    //m_swApp.ActiveDoc.ActiveView.FrameLeft = 20;
				    //m_swApp.ActiveDoc.ActiveView.FrameTop = 20;
				    //m_swApp.ActiveDoc.ActiveView.FrameWidth = m_bmpWidth;
				    //m_swApp.ActiveDoc.ActiveView.FrameHeight = m_bmpHeight;

                    m_swModelDoc.SetFeatureManagerWidth(0);

                    IModelView modelView = (IModelView)m_swModelDoc.ActiveView;
                    modelView.FrameState = 0;
                    modelView.FrameLeft  = 20;
                    modelView.FrameTop   = 20;
                    modelView.FrameWidth = m_bmpWidth;
                    modelView.FrameHeight= m_bmpHeight;

				    m_swModelDoc.SelectByID(sheetName, swSelSHEETS, 0, 0, 0);
				    m_swModelDoc.ViewZoomToSelection();
				    m_swModelDoc.GraphicsRedraw2();

				    sheetBMPPath = m_resultDataDir + m_fileName + "__" + m_phase + "_" + Convert.ToString(i + 1) + ".bmp";
				    bBMPCreatedFlag = m_swModelDoc.SaveBMP(sheetBMPPath, m_bmpWidth, m_bmpHeight);

				    sheetBMPNode = xmlDoc.createElement("BMPPath");
				    if (bBMPCreatedFlag)
				    {
					    sheetBMPNode.text = sheetBMPPath;
				    }
				    else
				    {
					    sheetBMPNode.text = "";
				    }
				    sheetNode.appendChild(sheetBMPNode);
			    }
                drawDocument.ActivateSheet(defaultSheetName);
			    tempGenerateDrawingXML = true;
			    goto func_cleanup;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:
            func_cleanup:
            return tempGenerateDrawingXML;
	    }


	    private bool GenerateModelDocumentXML(ref ModelDoc2 mDoc, ref MSXML2.DOMDocument60 xmlDoc, ref MSXML2.IXMLDOMNode xmlParentNode)
	    {
    	    bool tempGenerateModelDocumentXML = false;
		    try
		    {
			    tempGenerateModelDocumentXML = false;

			    MSXML2.IXMLDOMNode modelInfoNode = null;

			    modelInfoNode = xmlDoc.createElement("ModelDocumentInfo");
			    xmlParentNode.appendChild(modelInfoNode);

			    GenerateTessellationQualityXML(ref mDoc, ref xmlDoc, ref modelInfoNode);

			    tempGenerateModelDocumentXML = true;
			    goto func_cleanup;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:
            func_cleanup:
            return tempGenerateModelDocumentXML;
	    }

	    private bool GenerateTessellationQualityXML(ref ModelDoc2 mDoc, ref MSXML2.DOMDocument60 xmlDoc, ref MSXML2.IXMLDOMNode xmlParentNode)
	    {
			bool tempGenerateTessellationQualityXML = false;
		    try
		    {
			    tempGenerateTessellationQualityXML = false;

			    short tessQuality = 0;
			    MSXML2.IXMLDOMNode tessQualityNode = null;

                modTessellation.GetTessellationQuality(ref mDoc,ref tessQuality);

			    tessQualityNode = xmlDoc.createElement("TessellationQuality");
			    tessQualityNode.text = Convert.ToString(tessQuality);
			    xmlParentNode.appendChild(tessQualityNode);

			    tempGenerateTessellationQualityXML = true;
			    goto func_cleanup;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:
            func_cleanup:
            return tempGenerateTessellationQualityXML;
	    }

        private bool GenerateTessellationTrianglesCountXML(ref ModelDoc2 mDoc, ref MSXML2.DOMDocument60 xmlDoc, ref MSXML2.IXMLDOMNode xmlParentNode)
	    {
			bool tempGenerateTessellationTrianglesCountXML = false;
		    try
		    {
			    tempGenerateTessellationTrianglesCountXML = false;

			    int tessTriCount = 0;
			    MSXML2.IXMLDOMNode tessTriCountNode = null;

                modTessellation.GetTessellationTriangleCount(ref mDoc,ref tessTriCount);

			    tessTriCountNode = xmlDoc.createElement("NumTessellationTriangles");
			    tessTriCountNode.text = Convert.ToString(tessTriCount);
			    xmlParentNode.appendChild(tessTriCountNode);

			    tempGenerateTessellationTrianglesCountXML = true;
			    goto func_cleanup;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:
            func_cleanup:
            return tempGenerateTessellationTrianglesCountXML;
	    }

	    private bool GenerateMassPropertiesXML(ref ModelDoc2 mDoc, ref MSXML2.DOMDocument60 xmlDoc, ref MSXML2.IXMLDOMNode xmlParentNode)
	    {
			bool tempGenerateMassPropertiesXML = false;
            IModelDocExtension mDocExt = null;
            int massPropertiesStatus = 0;
            double[] massProperties = null;
            object massprop = null;
            MSXML2.IXMLDOMNode massPropertiesStatusNode = null;
            MSXML2.IXMLDOMNode massPropertiesNode = null;
            const Double dconst = 0.00000;


		    try
		    {
			    mDocExt = mDoc.Extension;
                massProperties = (double[])mDocExt.GetMassProperties(2,ref massPropertiesStatus);
                massprop = mDocExt.GetMassProperties(2, ref massPropertiesStatus);

			    massPropertiesNode = xmlDoc.createElement("MassProperties");
			    xmlParentNode.appendChild(massPropertiesNode);

			    massPropertiesStatusNode = xmlDoc.createElement("MassPropertiesStatus");
			    massPropertiesStatusNode.text = Convert.ToString(massPropertiesStatus);
			    massPropertiesNode.appendChild(massPropertiesStatusNode);

			    GenerateMassPropUnitsXML(ref mDoc, ref xmlDoc, ref massPropertiesNode);

			    MSXML2.IXMLDOMNode comXNode = null;
			    MSXML2.IXMLDOMNode comYNode = null;
			    MSXML2.IXMLDOMNode comZNode = null;
			    MSXML2.IXMLDOMNode volumeNode = null;
			    MSXML2.IXMLDOMNode areaNode = null;
			    MSXML2.IXMLDOMNode massNode = null;
			    MSXML2.IXMLDOMNode momXXNode = null;
			    MSXML2.IXMLDOMNode momYYNode = null;
			    MSXML2.IXMLDOMNode momZZNode = null;
			    MSXML2.IXMLDOMNode momXYNode = null;
			    MSXML2.IXMLDOMNode momZXNode = null;
			    MSXML2.IXMLDOMNode momYZNode = null;
			    MSXML2.IXMLDOMNode accuracyNode = null;
			    if (!((massProperties == null)))
			    {

				    comXNode = xmlDoc.createElement("CenterOfMassX");
                    Double number = massProperties[0] + dconst;
                    comXNode.text = number.ToString();
				    massPropertiesNode.appendChild(comXNode);

				    comYNode = xmlDoc.createElement("CenterOfMassY");
                    Double number2 = massProperties[1] + dconst;
                    comYNode.text = number2.ToString();
				    massPropertiesNode.appendChild(comYNode);

				    comZNode = xmlDoc.createElement("CenterOfMassZ");
                    Double number3 = massProperties[2] + dconst;
                    comZNode.text = number3.ToString();
				    massPropertiesNode.appendChild(comZNode);

				    volumeNode = xmlDoc.createElement("Volume");
                    volumeNode.text = massProperties[4].ToString();
				    massPropertiesNode.appendChild(volumeNode);

				    areaNode = xmlDoc.createElement("Area");
                    areaNode.text = massProperties[3].ToString();
                    massPropertiesNode.appendChild(areaNode);

				    massNode = xmlDoc.createElement("Mass");
                    massNode.text = massProperties[5].ToString();
                    massPropertiesNode.appendChild(massNode);

				    momXXNode = xmlDoc.createElement("MomXX");
                    momXXNode.text = massProperties[6].ToString("N5");
				    massPropertiesNode.appendChild(momXXNode);

				    momYYNode = xmlDoc.createElement("MomYY");
                    momYYNode.text = massProperties[7].ToString("N5");
				    massPropertiesNode.appendChild(momYYNode);

				    momZZNode = xmlDoc.createElement("MomZZ");
                    momZZNode.text = massProperties[8].ToString("N5");
				    massPropertiesNode.appendChild(momZZNode);

				    momXYNode = xmlDoc.createElement("MomXY");
                    momXYNode.text = massProperties[9].ToString("N5");
				    massPropertiesNode.appendChild(momXYNode);

				    momZXNode = xmlDoc.createElement("MomZX");
                    momZXNode.text = massProperties[10].ToString("N5");
				    massPropertiesNode.appendChild(momZXNode);

				    momYZNode = xmlDoc.createElement("MomYZ");
                    momYZNode.text = massProperties[11].ToString("N5");
				    massPropertiesNode.appendChild(momYZNode);

				    accuracyNode = xmlDoc.createElement("Accuracy");
                    accuracyNode.text = massProperties[12].ToString();
				    massPropertiesNode.appendChild(accuracyNode);
			    }
			    tempGenerateMassPropertiesXML = true;
			    goto func_cleanup;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:
            func_cleanup:
		            mDocExt = null;
		    return tempGenerateMassPropertiesXML;
	    }

	    private bool GenerateMassPropUnitsXML(ref ModelDoc2 mDoc, ref MSXML2.DOMDocument60 xmlDoc, ref MSXML2.IXMLDOMNode xmlParentNode)
	    {
			bool tempGenerateMassPropUnitsXML = false;
		    try
		    {
			    tempGenerateMassPropUnitsXML = false;
			    MSXML2.IXMLDOMNode massPropUnits = null;
			    massPropUnits = xmlDoc.createElement("MassPropUnits");

			    MSXML2.IXMLDOMNode decimalPlacesNode = null;
			    MSXML2.IXMLDOMNode lengthUnitNode = null;
			    MSXML2.IXMLDOMNode massUnitNode = null;
			    MSXML2.IXMLDOMNode volumeUnitNode = null;

			    decimalPlacesNode = xmlDoc.createElement("swUnitsMassPropDecimalPlaces");
			    decimalPlacesNode.text = Convert.ToString( mDoc.GetUserPreferenceIntegerValue((int)swUserPreferenceIntegerValue_e.swUnitsMassPropDecimalPlaces));
			    massPropUnits.appendChild(decimalPlacesNode);

			    lengthUnitNode = xmlDoc.createElement("swUnitsMassPropLength");
			    lengthUnitNode.text = Convert.ToString(mDoc.GetUserPreferenceIntegerValue((int)swUserPreferenceIntegerValue_e.swUnitsMassPropLength));
			    massPropUnits.appendChild(lengthUnitNode);

			    massUnitNode = xmlDoc.createElement("swUnitsMassPropMass");
			    massUnitNode.text = Convert.ToString(mDoc.GetUserPreferenceIntegerValue((int)swUserPreferenceIntegerValue_e.swUnitsMassPropMass));
			    massPropUnits.appendChild(massUnitNode);

			    volumeUnitNode = xmlDoc.createElement("swUnitsMassPropVolume");
			    volumeUnitNode.text = Convert.ToString( mDoc.GetUserPreferenceIntegerValue((int)swUserPreferenceIntegerValue_e.swUnitsMassPropVolume));
			    massPropUnits.appendChild(volumeUnitNode);

			    xmlParentNode.appendChild(massPropUnits);

			    tempGenerateMassPropUnitsXML = true;
			    goto func_cleanup;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:
            func_cleanup:
            return tempGenerateMassPropUnitsXML;
	    }

	    private bool GenerateModelFeaturesXML(ref ModelDoc2 mDoc, ref MSXML2.DOMDocument60 xmlDoc, ref MSXML2.IXMLDOMNode xmlParentNode)
	    {
			bool tempGenerateModelFeaturesXML = false;
            Feature feature = null;
		    try
		    {
			    tempGenerateModelFeaturesXML = false;

			    MSXML2.IXMLDOMNode featureCountNode = null;
			    MSXML2.IXMLDOMNode featureInfoNode = null;
			    int featureCount = 0;
			    featureInfoNode = xmlDoc.createElement("Features");

			    featureCountNode = xmlDoc.createElement("NumFeatures");
			    featureInfoNode.appendChild(featureCountNode);

			    featureCount = 0;
			    feature = mDoc.FirstFeature();

			    while (feature != null)
			    {
                    short i = 1;
				    GenerateFeatureXML(ref feature,ref i,ref featureCount,ref xmlDoc,ref featureInfoNode);
				    feature = feature.GetNextFeature();
			    }

			    featureCountNode.text = Convert.ToString(featureCount);
			    xmlParentNode.appendChild(featureInfoNode);
			    tempGenerateModelFeaturesXML = true;
			    goto func_cleanup;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:
            func_cleanup:
		    feature = null;
		    return tempGenerateModelFeaturesXML;
	    }


        private bool GenerateFeatureXML(ref Feature oFeature, ref short nLevel, ref int nPosition, ref MSXML2.DOMDocument60 xmlDoc, ref MSXML2.IXMLDOMNode xmlParentNode)
        {
			bool tempGenerateFeatureXML = false;
            modBOUtilities utils = new modBOUtilities();
            Feature subFeature = null;
		    try
		    {
			    tempGenerateFeatureXML = false;

			    MSXML2.IXMLDOMNode featureNode = null;
			    MSXML2.IXMLDOMAttribute featureNameAttribute = null;
			    MSXML2.IXMLDOMNode featureNameNode = null;
			    MSXML2.IXMLDOMNode featureTypeNameNode = null;
			    MSXML2.IXMLDOMNode featureSuppressNode = null;
			    MSXML2.IXMLDOMNode featureErrorNode = null;
			    MSXML2.IXMLDOMNode featurePositionNode = null;
			    MSXML2.IXMLDOMNode featureDepthNode = null;
			    MSXML2.IXMLDOMNode featureCountNode = null;
			    MSXML2.IXMLDOMNode featureInfoNode = null;
			    int featureCount = 0;
			    
			    string featureName = null;
			    int featureErrorCode = 0;

			    if (oFeature == null)
			    {
				    tempGenerateFeatureXML = true;
				    goto func_cleanup;
			    }

			    nPosition = nPosition + 1;
			    featureNode = xmlDoc.createElement("Feature");
                featureName = oFeature.Name;

			    featureNameAttribute = xmlDoc.createAttribute("name");
			    featureNameAttribute.value = featureName;
			    featureNode.attributes.setNamedItem(featureNameAttribute);

			    featureNameNode = xmlDoc.createElement("FeatureName");
			    featureNameNode.text = featureName;
			    featureNode.appendChild(featureNameNode);

			    featureTypeNameNode = xmlDoc.createElement("FeatureTypeName");
			    featureTypeNameNode.text = oFeature.GetTypeName();
			    featureNode.appendChild(featureTypeNameNode);

			    featureSuppressNode = xmlDoc.createElement("IsSuppressed");
                featureSuppressNode.text = utils.xmlBool(oFeature.IsSuppressed());
			    featureNode.appendChild(featureSuppressNode);

			    bool bIsWarning = false;

			    featureErrorCode = oFeature.GetErrorCode2(out bIsWarning);

			    featureErrorNode = xmlDoc.createElement("FeatureError");
			    featureErrorNode.text = Convert.ToString(featureErrorCode);
			    featureNode.appendChild(featureErrorNode);

			    featurePositionNode = xmlDoc.createElement("FeaturePosition");
			    featurePositionNode.text = Convert.ToString(nPosition);
			    featureNode.appendChild(featurePositionNode);

			    featureDepthNode = xmlDoc.createElement("FeatureDepth");
			    featureDepthNode.text = Convert.ToString(nLevel);
			    featureNode.appendChild(featureDepthNode);

			    //Record subfeatures
			    featureInfoNode = xmlDoc.createElement("Features");

			    featureCountNode = xmlDoc.createElement("NumFeatures");
			    featureInfoNode.appendChild(featureCountNode);

			    featureCount = 0;
			    subFeature = oFeature.GetFirstSubFeature();

			    while (subFeature != null)
			    {
				    var tempVar = nLevel + 1;
                    short tVar = (short)tempVar;
                    GenerateFeatureXML(ref subFeature, ref tVar, ref featureCount, ref xmlDoc, ref featureInfoNode);
				    subFeature = subFeature.GetNextSubFeature();
			    }

			    featureCountNode.text = Convert.ToString(featureCount);

			    featureNode.appendChild(featureInfoNode);
			    xmlParentNode.appendChild(featureNode);

			    tempGenerateFeatureXML = true;
			    goto func_cleanup;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:
            func_cleanup:
		            subFeature = null;
		    return tempGenerateFeatureXML;
	    }

	    private bool GenerateComponentsXML(ref Configuration configuration, ref MSXML2.DOMDocument60 xmlDoc, ref MSXML2.IXMLDOMNode xmlParentNode)
	    {
			bool tempGenerateComponentsXML = false;
            dynamic RootComponent = null;
            int i = 0;
            MSXML2.IXMLDOMNode componentCountNode = null;
            MSXML2.IXMLDOMNode componentInfoNode = null;
            int ComponentCount = 0;
            object[] components = null;
            modBOUtilities utils = new modBOUtilities();

		    try
		    {
			    RootComponent = configuration.GetRootComponent();
			    if (RootComponent == null)
			    {
				    ComponentCount = 0;
			    }
			    else
			    {
				    components = RootComponent.GetChildren;
				    ComponentCount = (int)components.GetUpperBound(0);
			    }

			    componentInfoNode = xmlDoc.createElement("Components");

			    componentCountNode = xmlDoc.createElement("NumComponents");
			    componentCountNode.text = Convert.ToString(ComponentCount);
			    componentInfoNode.appendChild(componentCountNode);

			    xmlParentNode.appendChild(componentInfoNode);

			    MSXML2.IXMLDOMNode componentNode = null;
			    MSXML2.IXMLDOMNode componentNameNode = null;
			    MSXML2.IXMLDOMNode componentPathNameNode = null;
			    MSXML2.IXMLDOMNode componentHiddenNode = null;
			    MSXML2.IXMLDOMNode componentSuppressNode = null;
			    MSXML2.IXMLDOMNode componentSuppressionStateNode = null;
			    //MSXML2.IXMLDOMNode componentErrorNode = null;
			    dynamic Component = null;
			    string componentName = null;
			    for (i = 0; i < ComponentCount; i++)
			    {

				    componentNode = xmlDoc.createElement("Component");
				    componentInfoNode.appendChild(componentNode);

				    Component = components[i];

				    componentName = Component.Name2();
				    componentNameNode = xmlDoc.createElement("ComponentName");
				    componentNameNode.text = componentName;
				    componentNode.appendChild(componentNameNode);

				    componentPathNameNode = xmlDoc.createElement("ComponentPathName");
				    componentPathNameNode.text = Component.GetPathName();
				    componentNode.appendChild(componentPathNameNode);

				    componentHiddenNode = xmlDoc.createElement("IsHidden");
                    componentHiddenNode.text = utils.xmlBool(Component.IsHidden(false));
				    componentNode.appendChild(componentHiddenNode);

				    componentSuppressNode = xmlDoc.createElement("IsSuppressed");
                    componentSuppressNode.text = utils.xmlBool(Component.IsSuppressed());
				    componentNode.appendChild(componentSuppressNode);

				    componentSuppressionStateNode = xmlDoc.createElement("SuppresionState");
                    componentSuppressionStateNode.text = Convert.ToString(Component.GetSuppression());
				    componentNode.appendChild(componentSuppressionStateNode);
			    }

			    tempGenerateComponentsXML = true;
			    goto func_cleanup;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:
            func_cleanup:
		            RootComponent = null;
		    return tempGenerateComponentsXML;
	    }

	    private bool GenerateQualityMetricsXML(ref MSXML2.DOMDocument60 xmlDoc, ref MSXML2.IXMLDOMNode xmlParentNode)
	    {
		    bool tempGenerateQualityMetricsXML = false;
		    try
		    {
			    MSXML2.IXMLDOMNode metricsRootNode = null;

			    metricsRootNode = xmlDoc.createElement("QualityMetrics");
			    xmlParentNode.appendChild(metricsRootNode);

			    GenerateDocumentStatusXML(ref xmlDoc, ref metricsRootNode);
			    GenerateDocumentPerformanceXML(ref xmlDoc, ref metricsRootNode);

			    tempGenerateQualityMetricsXML = true;
			    goto func_cleanup;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:
            func_cleanup:
            return tempGenerateQualityMetricsXML;
	    }

	    private bool GenerateDocumentStatusXML(ref MSXML2.DOMDocument60 xmlDoc, ref MSXML2.IXMLDOMNode xmlParentNode)
	    {
			bool tempGenerateDocumentStatusXML = false;
            modBOUtilities utils = new modBOUtilities();
		    try
		    {
			    MSXML2.IXMLDOMNode statusNode = null;
			    MSXML2.IXMLDOMNode dirtyNode = null;

			    statusNode = xmlDoc.createElement("DocumentStatus");
			    xmlParentNode.appendChild(statusNode);

			    dirtyNode = xmlDoc.createElement("OpenDirtyFlag");
                dirtyNode.text = utils.xmlBool(m_dirtyFlag);
			    statusNode.appendChild(dirtyNode);

			    tempGenerateDocumentStatusXML = true;
			    goto func_cleanup;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:
            func_cleanup:
            return tempGenerateDocumentStatusXML;
	    }

	    private bool GenerateDocumentPerformanceXML(ref MSXML2.DOMDocument60 xmlDoc, ref MSXML2.IXMLDOMNode xmlParentNode)
	    {
			bool tempGenerateDocumentPerformanceXML = false;
		    try
		    {
			    MSXML2.IXMLDOMNode performanceNode = null;
			    MSXML2.IXMLDOMNode openDocNode = null;
			    MSXML2.IXMLDOMNode saveDocNode = null;
			    MSXML2.IXMLDOMAttribute saveDocTimeUnitAttribute = null;
			    MSXML2.IXMLDOMAttribute openDocTimeUnitAttribute = null;

			    performanceNode = xmlDoc.createElement("DocumentPerformance");
			    xmlParentNode.appendChild(performanceNode);

			    openDocNode = xmlDoc.createElement("OpenDocumentTime");

			    openDocTimeUnitAttribute = xmlDoc.createAttribute("unit");
			    openDocTimeUnitAttribute.value = UNIT_MILLISECONDS;
			    openDocNode.attributes.setNamedItem(openDocTimeUnitAttribute);

			    openDocNode.text = Convert.ToString(GetTimerChange(OPEN_DOC_TIMER));
			    performanceNode.appendChild(openDocNode);

			    saveDocNode = xmlDoc.createElement("SaveDocumentTime");

			    saveDocTimeUnitAttribute = xmlDoc.createAttribute("unit");
			    saveDocTimeUnitAttribute.value = UNIT_MILLISECONDS;
			    saveDocNode.attributes.setNamedItem(saveDocTimeUnitAttribute);

			    saveDocNode.text = Convert.ToString(GetTimerChange(SAVE_DOC_TIMER));
			    performanceNode.appendChild(saveDocNode);

			    tempGenerateDocumentPerformanceXML = true;
			    goto func_cleanup;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:
            func_cleanup:
            return tempGenerateDocumentPerformanceXML;
	    }

	    private bool GenerateRebuildDocumentTimerXML(ref MSXML2.DOMDocument60 xmlDoc, ref MSXML2.IXMLDOMNode xmlParentNode)
	    {
    	    bool tempGenerateRebuildDocumentTimerXML = false;
            MSXML2.IXMLDOMNode rebuildDocNode = null;
            MSXML2.IXMLDOMAttribute rebuildDocTimeUnitAttribute = null;

		    try
		    {
			    rebuildDocNode = xmlDoc.createElement("RebuildDocumentTime");
			    rebuildDocTimeUnitAttribute = xmlDoc.createAttribute("unit");

			    rebuildDocTimeUnitAttribute.value = UNIT_MILLISECONDS;
			    rebuildDocNode.attributes.setNamedItem(rebuildDocTimeUnitAttribute);
			    rebuildDocNode.text = Convert.ToString(GetTimerChange(REBUILD_DOC_TIMER));
			    xmlParentNode.appendChild(rebuildDocNode);

			    tempGenerateRebuildDocumentTimerXML = true;
			    goto func_cleanup;
		    }
		    catch
		    {
			    goto error_handler;
		    }
            error_handler:
            func_cleanup:
		            rebuildDocNode = null;
		            rebuildDocTimeUnitAttribute = null;
		    return tempGenerateRebuildDocumentTimerXML;
	    }

	    //This functionality is not fully implemented
	    //Private Function GenerateDesignTableXML(ByRef mDoc As Object, _
	    //'                                        ByRef xmlDoc As MSXML2.DOMDocument60, _
	    //'                                        ByRef xmlParentNode As MSXML2.IXMLDOMNode) As Boolean
	    //    On Error GoTo error_handler
	    //    GenerateDesignTableXML = False
	    //
	    //    Dim designTableNode As MSXML2.IXMLDOMNode
	    //    Dim tempNode As MSXML2.IXMLDOMNode
	    //
	    //    Dim swmodel As Object
	    //    Dim swDesignTable As Object
	    //    Dim boolStatus As Boolean
	    //    Dim rows As Integer
	    //    Dim columns As Integer
	    //    Dim cells() As String
	    //
	    //    Set swmodel = mDoc
	    //
	    //    If swmodel.extension.HasDesignTable() Then
	    //        Set designTableNode = xmlDoc.createElement("DesignTable")
	    //        xmlParentNode.appendChild designTableNode
	    //
	    //        Set swDesignTable = swmodel.GetDesignTable()
	    //
	    //        swDesignTable.EditTable2 (False)
	    //
	    //        columns = swDesignTable.GetTotalColumnCount()
	    //        Set tempNode = xmlDoc.createElement("Columns")
	    //        tempNode.text = CStr(columns)
	    //        designTableNode.appendChild tempNode
	    //
	    //        rows = swDesignTable.GetTotalRowCount()
	    //        Set tempNode = xmlDoc.createElement("Rows")
	    //        tempNode.text = CStr(rows)
	    //        designTableNode.appendChild tempNode
	    //
	    //        If columns > 0 Then
	    //            ReDim cells(1 To columns) As String
	    //
	    //            Dim i As Integer
	    //
	    //            For i = 1 To columns
	    //                cells(i) = CStr(i)
	    //            Next i
	    //
	    //            boolStatus = swDesignTable.AddRow(cells)
	    //
	    //            Set tempNode = xmlDoc.createElement("AddRow")
	    //            tempNode.text = CStr(boolStatus)
	    //            designTableNode.appendChild tempNode
	    //        End If
	    //
	    //        boolStatus = swDesignTable.UpdateTable(swDesignTableUpdateOptions_e.swUpdateDesignTableSelected, False)
	    //
	    //        Set tempNode = xmlDoc.createElement("UpdateDesignTableSelected")
	    //        tempNode.text = CStr(boolStatus)
	    //        designTableNode.appendChild tempNode
	    //
	    //        boolStatus = swDesignTable.UpdateTable(swDesignTableUpdateOptions_e.swUpdateDesignTableAll, False)
	    //
	    //        Set tempNode = xmlDoc.createElement("UpdateDesignTableAll")
	    //        tempNode.text = CStr(boolStatus)
	    //        designTableNode.appendChild tempNode
	    //
	    //        boolStatus = swDesignTable.UpdateTable(swDesignTableUpdateOptions_e.swUpdateDesignTableNone, False)
	    //
	    //        Set tempNode = xmlDoc.createElement("UpdateDesignTableNone")
	    //        tempNode.text = CStr(boolStatus)
	    //        designTableNode.appendChild tempNode
	    //
	    //        swmodel.CloseFamilyTable
	    //    End If
	    //
	    //    GenerateDesignTableXML = True
	    //    GoTo func_cleanup
	    //error_handler:
	    //func_cleanup:
	    //    Set swmodel = Nothing
	    //    Set swDesignTable = Nothing
	    //    Set designTableNode = Nothing
	    //    Set tempNode = Nothing
	    //End Function

	    private void Class_Initialize_Renamed()
	    {
		    m_swApp = null;
		    m_swModelDoc = null;
		    m_debugLogger = new clsLogger();
		    m_warningLogger = new clsLogger();

		    m_debugLogger.IsEnabled = false;
		    m_warningLogger.IsEnabled = true;

		    m_startTimeDictionary = new Hashtable();
		    m_endTimeDictionary = new Hashtable();

		    m_dirtyFlag = false;
		    m_fileSize = -1;
		    m_bActivateAllConfigs = true;

		    m_bmpWidth = 800;
		    m_bmpHeight = 600;
	    }
        public clsQualityCheckTask(): base()
	    {
		    Class_Initialize_Renamed();
	    }

	    private void Class_Terminate_Renamed()
	    {
		    m_debugLogger = null;
		    m_warningLogger = null;
		    m_startTimeDictionary = null;
		    m_endTimeDictionary = null;
		    m_xmlRootNode = null;
		    m_xmlDoc = null;
		    m_swModelDoc = null;
		    m_swApp = null;
	    }
	    ~clsQualityCheckTask()
	    {
		    Class_Terminate_Renamed();
		    //base.Finalize();
	    }

    }//class
}//namespace

