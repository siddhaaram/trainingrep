//$c4  RS 12/12/13 Over loaded InitializesldSQLite with fileName. 
//$c3 QG8 10/21/13 Prj 19973 Removed DeInitializesldSQLite function.
//$c2 QG8 10/18/13 raw pointers replaced with Shared_ptr smart pointers
//$c1   QG8 10/17/13 Created;
//========================================================================//
//              Copyright 2013 (Unpublished Material)                     //
//                           SoildWorks Corporation                       //
//========================================================================//
//     File Name: sldSqliteMgr.h
//
//     Application: Generic SQLite Interface (C++ dll)
//
//     Contents: Defines Common Interface for SQLlite Manager.
//				(sldSqlite.dll)
//
//========================================================================//

#pragma once
#include "atlstr.h"
#include <memory>

#ifndef SLDSQLITE_INTERFACE_INCLUDED
#define SLDSQLITE_INTERFACE_INCLUDED

class I_sldSQLite; //Main SQlite wrapper Interface
class I_sldSQLiteTable; //SQLite table Interface. Client can perform operations on result
class I_sldSQLiteQuery; //SQLite Query Interface. 

//=================================== 
//Shared header between Dll and Client which defines the interface
//sldSqliteinterface.h 

class I_sldSQLite
{
public:
	virtual ~I_sldSQLite() = 0 {};
	
	virtual void connectSQLiteDB(CString strDBFileName) = 0; //for UTF8
	virtual void connectSQLiteDB_UTF16(CString strDBFileName) = 0; //for UTF16 - Costing uses
	virtual void connectSQLiteDB_Mode(CString strDBFileName, int Flags) = 0;// for specifying open mode

	virtual void closeSQLiteDB() = 0; //close the connection

	//returns the number of rows affected
	virtual int	 executeDML(CString strQuery) = 0; //for create/drop/insert/update/delete statements

	virtual std::shared_ptr<I_sldSQLiteQuery> getQueryResults(CString strQuery) = 0; //for Execute Query

	virtual bool compileStatement(CString strQueryStatmt) = 0; //Compile Statement

	virtual int  executeScalar(CString strQuery, int iNullValue) = 0; //to run a simple aggregate function

	virtual std::shared_ptr<I_sldSQLiteTable> getTable(CString strQuery) = 0; //Get Table

	virtual bool isTableExists(CString strTableName) = 0; //Look for Table Existence
	virtual void deleteTable(CString strTableName) = 0; //Look for Table Existence

	virtual void beginTransaction() = 0;
	virtual void commitTransaction() = 0;
	virtual void rollBack() = 0;

	virtual CString getSQLiteVersion() = 0;
	virtual CString getSQLiteHeaderVersion() = 0;
	virtual CString getSQLiteLibraryVersion() = 0;
	virtual int   getSQLiteLibraryVersionNumber() = 0;
};

class I_sldSQLiteTable
{
public:
	virtual int numFields() = 0;

	virtual int numRows() = 0;

	virtual CString fieldName(int nCol) = 0;

	virtual CString fieldValue(int nField) = 0;
	virtual CString fieldValue(const CString &strField) = 0;

	virtual int getIntField(int nField, int nNullValue=0) = 0;
	virtual int getIntField(const CString &szField, int nNullValue=0) = 0;

	virtual BOOL getBoolField(CString strField) = 0;

	virtual double getFloatField(int nField, double fNullValue=0.0) = 0;
	virtual double getFloatField(const CString &szField, double fNullValue=0.0) = 0;

	virtual CString getStringField(int nField, const CString &szNullValue) = 0;
	virtual CString getStringField(const CString &szField, const CString &szNullValue) = 0;

	virtual bool fieldIsNull(int nField) = 0;
	virtual bool fieldIsNull(const CString &szField) = 0;

	virtual void setRow(int nRow) = 0;

	virtual void finalize() = 0;
};

class I_sldSQLiteQuery
{
public:
	virtual int numFields() = 0;

	virtual int fieldIndex(const CString &szField) = 0;
	
	virtual const char* fieldName(int nCol) = 0;
	virtual const wchar_t* fieldNameUTF16(int nCol) = 0;

	virtual const char* fieldDeclType(int nCol) = 0;
	virtual const wchar_t* fieldDeclTypeUTF16(int nCol) = 0;

	virtual int fieldDataType(int nCol) = 0;

	virtual const char* fieldValue(int nField) = 0;
	virtual const wchar_t* fieldValueUTF16(int nField)=0;

	virtual const char* fieldValue(const CString &szField) = 0;
	virtual const wchar_t* fieldValueUTF16(const CString &szField) = 0;

	virtual int getIntField(int nField, int nNullValue=0) = 0;
	virtual int getIntField(const CString &szField, int nNullValue=0) = 0;

	//sqlite_int64 getInt64Field(int nField, sqlite_int64 nNullValue=0) = 0;
	//sqlite_int64 getInt64Field(const char* szField, sqlite_int64 nNullValue=0) = 0;

	virtual double getFloatField(int nField, double fNullValue=0.0) = 0;
	virtual double getFloatField(const CString &szField, double fNullValue=0.0) = 0;

	virtual const char* getStringField(int nField, const CString &szNullValue) = 0;
	virtual const wchar_t* getStringFieldUTF16(int nField, const CString &szNullValue/*=""*/) = 0;

	virtual const char* getStringField(const CString &szField, const CString &szNullValue) = 0;
	virtual const wchar_t* getStringFieldUTF16(const CString &szField, const CString &szNullValue/*=""*/) = 0;

	virtual const unsigned char* getBlobField(int nField, int& nLen) = 0;
	virtual const unsigned char* getBlobField(const CString &szField, int& nLen) = 0;

	virtual bool fieldIsNull(int nField) = 0;
	virtual bool fieldIsNull(const CString &szField) = 0;

	virtual bool eof() = 0;

	virtual void nextRow() = 0;

	virtual void finalize() = 0;
};

class sldSQLiteException_c
{
private:
	int m_iErrorCode;
	CString m_strErrorMessage;

	CString getErrorCodeAsString(int m_strErrorMessage);

public:
	sldSQLiteException_c(int nErrCode, CString szErrorMessage);
	sldSQLiteException_c(const sldSQLiteException_c&  e);

	~sldSQLiteException_c(){}
	int getErrorCode() { return m_iErrorCode; }
	CString getErrorMessage() { return m_strErrorMessage; } 
};

extern "C" 
{
	BOOL  GetSQLiteinterface(std::shared_ptr<I_sldSQLite> &pinterface);
	typedef BOOL (*GET_SQLITE_INTERFACE)(std::shared_ptr<I_sldSQLite> &pinterface);
}

namespace SldSqlite
{
	inline BOOL InitializesldSQLite(std::shared_ptr<I_sldSQLite> &pInterface, CString filePathIn = _T(""))
	{
		HINSTANCE		g_hInstance = NULL;
		GET_SQLITE_INTERFACE	g_pfnGetSqliteInterface= NULL;

		CString appPath(filePathIn);

		if (appPath.IsEmpty())
		{
			TCHAR szExePath[_MAX_PATH] = { 0 };
			GetModuleFileName(NULL, szExePath, _MAX_PATH);
			TCHAR * pc = _tcsrchr(szExePath, '\\');
			if (pc) {
				*pc = '\0';
			}
			appPath =  (CString)szExePath + _T("\\sldSqlite.dll");
		}

		g_hInstance = ::LoadLibrary(appPath);

		if(!g_hInstance)
			return FALSE;

		if(!g_pfnGetSqliteInterface)
			g_pfnGetSqliteInterface = (GET_SQLITE_INTERFACE) GetProcAddress(g_hInstance, 
			"GetSQLiteInterface");

		if(g_pfnGetSqliteInterface == NULL)
		{		
			::FreeLibrary(g_hInstance);
			return FALSE;
		}

		BOOL bSuccess = g_pfnGetSqliteInterface(pInterface);
		
		if(!pInterface || !bSuccess)
		{
			::FreeLibrary(g_hInstance);
			return FALSE;
		}
		return TRUE;
	}
}

/*
** CAPI3REF: Flags For File Open Operations
**
** These bit values are intended for use in the
** 3rd parameter to the [sqlite3_open_v2()] interface and
** in the 4th parameter to the [sqlite3_vfs.xOpen] method.
*/
#define SQLITE_OPEN_READONLY         0x00000001  /* Ok for sqlite3_open_v2() */
#define SQLITE_OPEN_READWRITE        0x00000002  /* Ok for sqlite3_open_v2() */
#define SQLITE_OPEN_CREATE           0x00000004  /* Ok for sqlite3_open_v2() */
#define SQLITE_OPEN_DELETEONCLOSE    0x00000008  /* VFS only */
#define SQLITE_OPEN_EXCLUSIVE        0x00000010  /* VFS only */
#define SQLITE_OPEN_AUTOPROXY        0x00000020  /* VFS only */
#define SQLITE_OPEN_URI              0x00000040  /* Ok for sqlite3_open_v2() */
#define SQLITE_OPEN_MAIN_DB          0x00000100  /* VFS only */
#define SQLITE_OPEN_TEMP_DB          0x00000200  /* VFS only */
#define SQLITE_OPEN_TRANSIENT_DB     0x00000400  /* VFS only */
#define SQLITE_OPEN_MAIN_JOURNAL     0x00000800  /* VFS only */
#define SQLITE_OPEN_TEMP_JOURNAL     0x00001000  /* VFS only */
#define SQLITE_OPEN_SUBJOURNAL       0x00002000  /* VFS only */
#define SQLITE_OPEN_MASTER_JOURNAL   0x00004000  /* VFS only */
#define SQLITE_OPEN_NOMUTEX          0x00008000  /* Ok for sqlite3_open_v2() */
#define SQLITE_OPEN_FULLMUTEX        0x00010000  /* Ok for sqlite3_open_v2() */
#define SQLITE_OPEN_SHAREDCACHE      0x00020000  /* Ok for sqlite3_open_v2() */
#define SQLITE_OPEN_PRIVATECACHE     0x00040000  /* Ok for sqlite3_open_v2() */
#define SQLITE_OPEN_WAL              0x00080000  /* VFS only */

#endif //SLDSQLITE_INTERFACE_INCLUDED